﻿using Microsoft.AspNetCore.Mvc;

namespace TextAppWithMVC.Controllers
{
    public class SendTextController : Controller
    {
        [HttpGet]
        public IActionResult Page()
        {
            ViewBag.Message = "";
            return View();
        }

        [HttpPost]
        public IActionResult Page(string text)
        {
            ViewBag.Message = (text == null) ? "Ошибка отправки!" : text + " успешно отправлен!";
            return View();
        }

    }
}